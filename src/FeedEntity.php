<?php

namespace Parangon\Feed2go;

class FeedEntity implements FeedEntityInterface
{
    use FeedUtils;

    protected object $feed;
    protected $field;

    public string $id;
    public string $type;
    public string $slug;
    public string $lang;
    public bool $isPreview;

    public function __construct($feed, $isPreview = false)
    {
        $this->feed = $feed;
        $this->isPreview = $isPreview;

        $this->id   = $feed->infos->id ?? "";
        $this->type = $feed->infos->type ?? "";
        $this->slug = $feed->infos->slug ?? "";
        $this->lang = $feed->infos->language ?? "";
    }

    public function parse(?string $args = null)
    {
        $this->field = $this->feed;

        if ($args) {
            $args = explode(".", $args);
            foreach ($args as $arg) {
                $this->field = $this->field->{$arg} ?? null;
                if ($this->field === null) break;
            }
        }

        return $this;
    }

    public function get(string $args, string $separator = "")
    {
        $this->parse($args);

        switch (true) {
            case empty($this->field):
                return "";
            case is_string($this->field) && preg_match("/(\.jpg|\.jpeg|\.png|\.pdf|\.tif|\.tiff|\.gif)$/", strtolower($this->field)) > 0:
                return $this->toSrc();
            case !empty($separator) && is_string($this->field):
                return $this->toArray($separator);
            case is_string($this->field) || is_numeric($this->field):
                return $this->toString();
            case is_array($this->field) || is_object($this->field):
                return $this->asArray();
            default:
                return $this->toRaw();
        }
    }

    public function toString(?string $field = null): string
    {
        $field = $field ?? $this->field;
        return isset($field) && (is_string($field) || is_numeric($field)) ? trim(html_entity_decode($field)) : "";
    }

    public function toSrc(?string $field = null): string
    {
        $src = $field ?? $this->field;
        return isset($src) && is_string($src) && !empty($src) ? $this->feed->server->images . $src : $this->feed->server->url . "/sources/img/broken-image.png";
    }

    public function toArray(string $separator = ";", ?string $field = null): array
    {
        $field = $field ?? $this->field;
        if(is_string($field) && !empty($field)) {
            $entries = explode($separator, $field);
            foreach($entries as &$entry) $entry = $this->toString($entry);
            return $entries;
        } else {
            return [];
        }
    }

    public function asArray($field = null): array
    {
        $field = $field ?? $this->field;
        return (is_array($field) || is_object($field)) && !empty($field) ? (array)$field : [];
    }

    public function toRaw($field = null)
    {
        return $field ?? $this->field;
    }
}