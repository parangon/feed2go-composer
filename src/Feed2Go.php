<?php
/**
 * Feed2go library (03.2024)
 * @Documentation https://api.castel-freres.net/v4/doc/roadmap/feed2go
 **/
namespace Parangon\Feed2go;

use Dotenv\Dotenv;
use Exception;
use Parangon\Page2go\Page2Go;
use Parangon\Page2go\Templating\PageModels;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Feed2Go
{
    /** @var string api url */
    private string $apiUrl;

    /** @var string api token */
    private string $apiToken;

    /** @var string auto detect in __construct */
    private string $locale = 'fr';

    /** @var array default filters fetchFeeds */
    private array $defaultFilters = ["orderBy" => "autoPublication", "sort" => "DESC", "limit" => 100];

    /** @var FeedEntity[] */
    private array $feeds = [];

    /** @var FeedEntity|null */
    private ?FeedEntity $feed;

    /** @var object|array */
    public $pages = [];

    /*** @throws Exception */
    public function __construct(?string $apiUrl = null, ?string $apiToken = null)
    {
        $env = Dotenv::createMutable(dirname(__DIR__, 4) . "/");
        $this->apiUrl   = $apiUrl ?? $env->load()['FEED_API_URL'];
        $this->apiToken = $apiToken ?? $env->load()['FEED_API_TOKEN'];

        $isSetLocale = preg_match("/^\/([a-zA-Z]{2}($|\/)|[a-z]{2}-[A-Z]{2}($|\/))/", $_SERVER['REQUEST_URI'], $matches) > 0;
        $this->setLocale($isSetLocale ? str_replace("/", "", $matches[0]) : null);
    }

    /**
     * @param string|null $locale
     * @return Feed2Go
     */
    public function setLocale(?string $locale = null): Feed2Go
    {
        $this->locale = $locale ?? $this->locale;
        return $this;
    }

    /** @return string */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param array $filters
     * @return Feed2Go
     */
    public function setDefaultFilters(array $filters): Feed2Go
    {
        $this->defaultFilters = $filters;
        return $this;
    }

    /** @return FeedEntity */
    public function feed(): ?FeedEntity
    {
        return $this->feed ?? $this->feeds[0] ?? null;
    }

    /** @return FeedEntity[] */
    public function feeds(): array
    {
        return $this->feeds;
    }

    /**
     * Use Page2go library
     * @Documentation https://gitlab.com/parangon/page2go-composer.git
     * @param array $options
     * @param $templating
     * @param bool $withCSS
     * @param bool $withJS
     * @return string
     */
    public function page2go(array $options = [], $templating = PageModels::PAGE2GO_LINK, bool $withCSS = true, bool $withJS = true): string
    {
        try {
            return (new Page2Go($this->pages, $options, $templating))->load($withCSS, $withJS);
        } catch (Exception $e) {
            return "";
        }
    }

    /**
     * @param string $type
     * @param array $filters
     * @return FeedEntity[]
     * @throws Exception
     */
    public function fetchRandom(string $type = "all", array $filters = []): array
    {
        return $this->fetchFeeds($type, array_merge(['random' => true], $filters));
    }

    /**
     * @param string $type
     * @param array $filters
     * @return FeedEntity[]
     * @throws Exception
     */
    public function fetchFeeds(string $type = "all", array $filters = []): array
    {
        $filters  = array_merge($this->defaultFilters, $filters);
        $qFilters = http_build_query($filters);

        $response = $this->fetch("/feeds/{$type}/{$this->locale}/?{$qFilters}");
        $data = json_decode($response->getContent() ?? '[]', false);

        if(isset($data->feeds) && isset($data->pages)) {
            $this->feeds = $data->feeds;
            $this->pages = $data->pages;
        } else {
            $this->feeds = $data;
            $this->pages = [];
        }

        foreach ($this->feeds as &$feed) $feed = new FeedEntity($feed);

        return $this->feeds;
    }

    /**
     * @param string $slug
     * @return FeedEntity
     * @throws Exception
     */
    public function fetchFeed(string $slug): FeedEntity
    {
        try {
            if(preg_match("/(\..+$|\..+\/.+)/", $slug, $matches) > 0) {
                $slug = str_replace($matches[0], "", $slug);
            }

            $isPreview = preg_match("/\/preview$/", $slug) > 0;
            $slug = str_replace("/preview", "", $slug);

            $response = $this->fetch("/feed/{$this->locale}/{$slug}");
            $content = $response->getContent();

            if(empty($content)) {
                throw new Exception("error fetching: $slug", $response->getStatusCode());
            }

            $feed = json_decode($content, false);
            $this->feed = new FeedEntity($feed, $isPreview);

            return $this->feed;
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param string $uri
     * @return ResponseInterface
     * @throws Exception
     */
    private function fetch(string $uri): ResponseInterface
    {
        try {
            $client = HttpClient::create();
            $response = $client->request(
                "GET", "{$this->apiUrl}{$uri}",
                ['auth_bearer' => $this->apiToken]
            );

            if($response->getStatusCode() !== 200) {
                throw new Exception("error fetching: $uri", $response->getStatusCode());
            }

            return $response;
        } catch (Exception|TransportExceptionInterface $e) {
            throw new Exception($e);
        }
    }
}