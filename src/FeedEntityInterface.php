<?php

namespace Parangon\Feed2go;

interface FeedEntityInterface
{
    public function parse(?string $args = null);
    public function get(string $args, string $separator = "");
    public function toString(?string $field = null);
    public function toSrc(?string $field = null);
    public function toArray(string $separator = ";", ?string $field = null);
    public function asArray($field = null);
    public function toRaw($field = null);
}