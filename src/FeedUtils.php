<?php

namespace Parangon\Feed2go;

trait FeedUtils
{
    public function dd(?string $arg = null): void
    {
        $this->dump($arg, true);
    }

    public function dump(?string $arg = null, $die = false): void
    {
        $this->parse($arg);

        echo "<pre>";
        var_dump($this->field);
        echo "</pre>";

        if($die) die;
    }

    public function swagger(?string $bgColor = "#d3d3d35c"): void
    {
        $textColor = $this->getContrastColor($bgColor);

        echo "<style>pre.swagger{position:sticky;top:1em;left:1em;z-index:100000;background-color:$bgColor;color:$textColor;white-space:break-spaces;backdrop-filter:blur(15px);border-radius:8px;width:auto;max-width:400px;padding:20px;box-shadow:1px 5px 20px #0000008c;} pre.swagger div{padding:5px 0}</style>";
        echo "<pre class='swagger'><div>Feed N°{$this->id}&nbsp;/{$this->lang}/{$this->type}/{$this->slug}</div>";
        foreach($this->feed as $index => $value) {
            echo "<div>[$index]</div>";
            $this->echoKeys((array)$value, $index);
        }
        echo "</pre>";
    }

    private function echoKeys($values, $index): void
    {
        echo "<ul style='margin:0 0 10px 10px;'>";
        foreach ($values as $k => $value) {
            echo "<li style='list-style:none;'>$index.$k".(is_array($value) && isset($value[0]) ? "[]" : "")."</li>";
            if(is_object($value)) $this->echoKeys($value, "$index.$k");
        }
        echo "</ul>";
    }

    private function getContrastColor(string $hex): string
    {
        $r = hexdec(substr($hex, 1, 2));
        $g = hexdec(substr($hex, 3, 2));
        $b = hexdec(substr($hex, 5, 2));
        $yiq = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;
        return ($yiq >= 128) ? 'black' : 'white';
    }
}