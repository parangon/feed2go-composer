### parangon/feed2go

* Feed2go library (03.2024)
* documentation https://api.castel-freres.net/v4/doc/roadmap/feed2go


### INSTALL
```
composer.json
```
```
{
    "require": {
            "parangon/feed2go": "1.*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/parangon/feed2go-composer.git"
        },
        {
            "type": "vcs",
            "url": "https://gitlab.com/parangon/page2go-composer.git"
        }
    ]
}
```

##### Environment vars
```
.env
```
```
FEED_API_URL="https_api_dot_com"
FEED_API_TOKEN="auth_bearer_token"
```

##### Prevent access to .env file
```
.htaccess 
```
```
Options -Indexes

<Files .env>
    Order allow,deny
    Deny from all
</Files>
```

###
### Playground
```
/*** Fetch Feed(s) */
require_once "./vendor/autoload.php";
use Parangon\Feed2go\Feed2Go;

try {
    $feed2go = new Feed2Go();

    /*** with params */
    // $feed2go = (new Feed2Go())->setLocale('fr')->setDefaultFilters([]);

    /*** Fetch multiple (/feeds/type/language?filters[]) */
    $feeds = $feed2go->fetchFeeds('all', ['orderBy' => 'id', 'sort' => 'ASC']);

    /*** Fetch random */
    // $feeds = $feed2go->fetchRandom('type', ['filters[option]' => 'true']);

    /*** Fetch one (/feed/language/slug) */
    // $feed = $feed2go->fetchFeed('slug');

    /*** Prompt feed's swagger */
    $feeds[0]->swagger();

    /*** Dump Die Feed */
    $feeds[0]->dd();
} catch (Exception $e) {
    header("location: /404.php");
    exit;
}
```
```
/*** Use $feeds OR $feed */
foreach($feeds as $feed) 
{
    // fields set automatically
    echo $feed->id;
    echo $feed->type;
    echo $feed->slug;
    echo $feed->lang;
    echo $feed->isPreview;
    
    /*** call other fields with auto-convertion */
    echo $feed->get('main.title');
    
    /*** auto-convert image as src link when field ends with jpg|jpeg|png|pdf|tif|tiff|gif */
    /*** automatically concats: $feed->get('server.images') . $feed->get('share.thumb') */
    echo $feed->get('share.thumb');
}
```
```
/*** Parse array fields like feed->main->rows[] */
foreach($feed->get('main.rows') as $row) {
    echo $feed->toSrc($row->image);
    echo $feed->toString($row->text);
}

/*** Or text with semicolon */
foreach($feed->get('main.textSemicolon', ";") as $text) {
    echo $feed->toString($text);
}
```
```
/*** Bind auto-convert */
$feed->parse('main.title')->toString();
$feed->parse('main.fullImage')->toSrc();
$feed->parse('main.textSemicolon')->toArray(";");
$feed->parse('main.categories')->asArray();
$feed->parse('main')->toRaw();
```


#### More
```
/** 
 * Use parangon/page2go for pagination 
 * @Documentation https://gitlab.com/parangon/page2go-composer.git 
*/
<div id="pagination"><?= $feed2go->page2go(); ?></div>
```
<br>
contact : dev@parangon-creations.com